#include "game_scene.h"
#include <math.h>

#define TILESIZE 10
#define COLS_IN_SCREEN (WIDTH / TILESIZE)
#define ROWS_IN_SCREEN (HEIGHT / TILESIZE)
#define COLS 100
#define ROWS 100
#define NUM_ISLANDS 10

#define NUM_FOOD 10
#define NUM_ENEMY 10
#define NUM_ENERGY2 5
#define NUM_POISON 5
#define NUM_ITEMS 30

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))
#define WORLD(x,y) (world[(y) * COLS + (x)])
#define CHECK_TYPE(x,y,t) ((WORLD((x), (y)) & (t)) > 0)

#define MINIMAP_ZOOM 4
#define MINIMAP_X (WIDTH - (COLS / MINIMAP_ZOOM + 10))
#define MINIMAP_Y (HEIGHT - (ROWS / MINIMAP_ZOOM + 10))


extern Texture *tex_font[26], *tex_logo;
extern Texture *tile_textures[3];

typedef struct{
	int r,g,b;
} RGB;

typedef struct{
	int x, y, w, h;
} BB;

typedef struct{
	int ox, oy;
	BB bb;
} Island;
Island islands[300];

struct player{
	int x, y;
	float energy, max_energy;
	int area;
	int type;
	RGB color;
} player = {.color = {230,230,230}};

static double t;	

struct camera{
	float x, y;
	float goalx, goaly;
	float speed;
	bool flying;
} camera;


typedef enum {
	NONE, POSITIVE, NEGATIVE, ENEMY, FOOD, ENERGY2, POISON, TILE_COUNT 
} TileType;

static RGB TILE_COLORS[] = {
/*	{5, 5, 5},
	{160, 20, 20},
	{50, 50, 50},*/
	{100, 100, 100},
	{255, 255, 255},
	{180, 180, 180},
	{255, 0, 0},
	{255, 180, 0},
	{180, 180, 180},
	{250, 0, 200}
};

static RGB MINIMAP_COLORS[] = {
	{80, 80, 80},
	{230, 100, 100},
	{170, 170, 170},
	{255, 0, 0},
	{255, 180, 0},
	{180, 180, 180},
	{250, 0, 200}
};

static unsigned char world[COLS * ROWS];

struct {
	int ax, ay;
	int bx, by;
	int area_a, area_b;
} portal;

struct items{
	int x, y;
	TileType type;
	float energy;
} items[NUM_ITEMS];


double poison_time = 0; //motion blur countdown
double logo_alpha;


static void win(){
	poison_time = 0;
	printf("WIN!!!\n");
	t = 0;
}

static void gameover(){
	poison_time = 0;
	printf("GAME OVER :(\n");
	t = 0;
}

void drawText(char *text, int x, int y, bool centered, int r, int g, int b, int a){
	int len = strlen(text);
	if (centered){
		x = WIDTH2 - ((len-1) / 2 * 10);
	}
	for (int i = 0; i < len; i++) {
		if (text[i] == ' '){x += 5; continue;}
		drawQuad(tex_font[text[i] - 'A'], x, y, 10, 10, r, g, b, a, 1.0, 1.0);
		x += 10;
	}
}

/*
static void printWorld(){
	printf("- world ---\n");
	printf(" \t");
	for (int x = 0; x < COLS; ++x) {
		printf("%i ", x%10);
	}
	printf("\n\n");
	for (int y = 0; y < ROWS; ++y){
		printf("%i\t", y);
 		for (int x = 0; x < COLS; ++x) {
			printf("%i ", WORLD(x, y));
		}
		printf("\n");
	}
}*/

static bool rand_no_tiletype(TileType type, int *x, int *y){
	int i;
	do {
		*x = rand() % COLS;
		*y = rand() % ROWS;
		if (WORLD(*x, *y) != type) return true;
	} while(i++ < 10000);
	return false;
}

static bool rand_with_tiletype(TileType type, int *x, int *y){
	int i = 0;
	do {
		*x = rand() % COLS;
		*y = rand() % ROWS;
		if (WORLD(*x, *y) == type) return true;
	} while(i++ < 10000);
	return false;
}

static bool rand_in_area(TileType area_type, BB *bb, int *x, int *y){
	int i = 0;
	//printf("-- BB %i %i, %i %i\n", bb->x, bb->y, bb->w, bb->h);
	do {
		*x = bb->x + (rand() % bb->w);
		*y = bb->y + (rand() % bb->h);
		if ((WORLD(*x, *y) & area_type) > 0) return true;
	} while(i++ < 10000);
	return false;
}

int check_item_in(int x, int y){
	for (int i = 0; i < NUM_ITEMS; i++) {
		if (items[i].x == x && items[i].y == y && items[i].energy > 0) return i;
	}
	return -1;
}


static void grow_area(int x, int y, TileType tile_type, int energy, BB *bb){
	if (x < 0 || x >= COLS || y < 0 || y >= ROWS) return;
	if (energy < 0 || WORLD(x, y) == tile_type) return;
	WORLD(x, y) = tile_type;

	//if (bb && energy == 10) printf("grow from %i %i\n", x, y);
	if (bb){	
		if (x < bb->x) bb->x = x;
		if (y < bb->y) bb->y = y;
		if (x > bb->x + bb->w) bb->w = x - bb->x;
		if (y > bb->y + bb->h) bb->h = y - bb->y;
	}

	grow_area(x + 1, y, tile_type, energy - 1 - rand() % 2, bb);
	grow_area(x - 1, y, tile_type, energy - 1 - rand() % 2, bb);
  grow_area(x, y + 1, tile_type, energy - 1 - rand() % 2, bb);
	grow_area(x, y - 1, tile_type, energy - 1 - rand() % 2, bb);
	energy -= 1;
	if (energy < 0) return;
	grow_area(x - 1, y + 1, tile_type, energy - rand() % 3, bb);
	grow_area(x + 1, y - 1, tile_type, energy - rand() % 3, bb);
	grow_area(x - 1, y - 1, tile_type, energy - rand() % 3, bb);
  grow_area(x + 1, y + 1, tile_type, energy - rand() % 3, bb);
}

static void regenerate_portal(int player_area){
	int x, y;
	do{
		rand_in_area(NEGATIVE|POSITIVE, &(islands[player_area].bb), &x, &y);
	} while (check_item_in(x, y) != -1 || (x == player.x && y == player.y));

	portal.ax = x;
	portal.ay = y;
	portal.area_a = player_area;
	
	int j = player_area + 1;
	if (j == NUM_ISLANDS){
		win();
		return;
	}
	do{
		rand_in_area(POSITIVE|NEGATIVE, &(islands[j].bb), &x, &y); 
	}while(check_item_in(x, y) != -1);

	portal.bx = x;
	portal.by = y;
	portal.area_b = j;
	//printf("portal %i %i -> %i %i\n", portal.ax, portal.ay, portal.bx, portal.by);
}

static void generate_world(){
	memset(world, NONE, COLS * ROWS);
	int num_islands = NUM_ISLANDS, areas_per_island = 3;
	int x, y;
	int i, j;
	for (i = 0; i < num_islands; i++) {
		BB bb = {COLS, ROWS, 0, 0};
		rand_no_tiletype(POSITIVE, &x, &y);
		grow_area(x, y, POSITIVE, 10, &bb);
		//printf("area %i BB %i %i, %i %i\n", i, bb.x, bb.y, bb.w, bb.h);

		islands[i].ox = x;
		islands[i].oy = y;
		islands[i].bb.x = bb.x;
		islands[i].bb.y = bb.y;
		islands[i].bb.w = bb.w;
		islands[i].bb.h = bb.h;

		for (j = 0; j < areas_per_island; j++) {
			rand_with_tiletype(POSITIVE, &x, &y);
			grow_area(x, y, NEGATIVE, 2, NULL);
		}
	}
	//printWorld();
}

static void generate_items(){ 
	int x, y, island, j = 0;
	const int itemtypes[4] = {FOOD, ENEMY, ENERGY2, POISON};
	const int itemcount[4] = {NUM_FOOD, NUM_ENEMY, NUM_ENERGY2, NUM_POISON};
	for (int t = 0; t < 4; t++) {
		for (int i = 0; i < itemcount[t]; i++) {
			island = rand() % NUM_ISLANDS;
			do{
				rand_in_area(POSITIVE|NEGATIVE, &(islands[island].bb), &x, &y);
			} while( 
				(portal.ax == x && portal.ay == y) || 
				(portal.bx == x && portal.by == y) ||
				(player.x == x && player.y == y));
			items[j].x = x;
			items[j].y = y;
			items[j].type = itemtypes[t];
			items[j].energy = 0.1f + (float)(rand() % 9) / 10.f;
			j++;
		}
	}
}

void move_camera_to(int x, int y, bool fly){
	camera.goalx = x;
	camera.goaly = y;
	if (x < 0) camera.goalx = 0;
	if (y < 0) camera.goaly = 0;
	if (x > COLS - COLS_IN_SCREEN) camera.goalx = COLS - COLS_IN_SCREEN;
	if (y > ROWS - ROWS_IN_SCREEN) camera.goaly = ROWS - ROWS_IN_SCREEN;
	if (!fly){
		camera.x = camera.goalx;
		camera.y = camera.goaly;
		camera.speed = 1.f;
		camera.flying = false;
	}
	else{
		float dx = camera.goalx - camera.x, dy = camera.goaly - camera.y;
		camera.speed = 0.1 - (sqrt(dx * dx + dy * dy) / COLS) * 0.1;
		camera.flying = true;
	}
	//printf("camera %f %f -> %f %f at %f\n", camera.x, camera.y, camera.goalx, camera.goaly, camera.speed);
}

void move_camera_by(int dx, int dy, bool fly){
	move_camera_to(camera.x + dx, camera.y + dy, fly);
}

void center_camera(int x, int y, bool fly){
	move_camera_to(x - COLS_IN_SCREEN / 2, y - ROWS_IN_SCREEN / 2, fly);
}

void move_player(int dx, int dy){
	int item;
	int px = player.x + dx;
	int py = player.y + dy;
	
	logo_alpha -= 0.1;

	if (px < 0 || py < 0 || px >= COLS || py >= ROWS) return;

	if (WORLD(px, py) == NONE) return;

	player.energy -= 0.1;
	if (px == portal.ax && py == portal.ay){
		px = portal.bx;
		py = portal.by;
		player.area = portal.area_b;
		player.energy = player.max_energy;
		regenerate_portal(portal.area_b);
	}
	else if ((item = check_item_in(px, py)) != -1) {
		switch(items[item].type){
			case POISON:
				player.energy -= items[item].energy;
				items[item].energy = 0.f;
				poison_time = 5.0;
			break;
			case FOOD:
				player.energy += items[item].energy;
				if (player.energy > player.max_energy) player.energy = player.max_energy;
				items[item].energy = 0.f;
			break;
			case ENEMY:
				player.energy -= items[item].energy;
				items[item].energy -= 0.5f;
				px -= dx;
				py -= dy;
			break;
			case ENERGY2:
				player.max_energy += 1.0;
				items[item].energy = 0.f;
			break;
			default:
			break;
		}
	}
	else if ((WORLD(px, py) == NEGATIVE || WORLD(px, py) == POSITIVE ) && WORLD(px, py) != player.type) {
		player.energy = player.max_energy;
		player.type = WORLD(px, py);
	}

	if (player.energy <= 0){ gameover(); return; }

	player.x = px;
	player.y = py;
	player.type = WORLD(px, py);
	center_camera(player.x, player.y, true);
}

void gamescene_processEvents(GLFWwindow *window){
	if (keyJustPressed(GLFW_KEY_Q)){
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (keyJustPressed(GLFW_KEY_F5)){
		t = 0;
	}
	if (keyPressed(GLFW_KEY_W)) {move_camera_by(0, -1, false);}
	if (keyPressed(GLFW_KEY_A)) {move_camera_by(-1, 0, false);}
	if (keyPressed(GLFW_KEY_S)) {move_camera_by(0, 1, false);}
	if (keyPressed(GLFW_KEY_D)) {move_camera_by(1, 0, false);}

	if (keyJustPressed(GLFW_KEY_LEFT)) { move_player(-1, 0); }
	if (keyJustPressed(GLFW_KEY_RIGHT)){ move_player(1, 0); }
	if (keyJustPressed(GLFW_KEY_UP))   { move_player(0, -1); }
	if (keyJustPressed(GLFW_KEY_DOWN)) { move_player(0, 1); }

}

int var = 0;

void start_game(double time){
	logo_alpha = 2.0;
  srand((int)(getMouseX() * time));
  generate_world();
  player.area = 0;
	rand_in_area(POSITIVE|NEGATIVE, &(islands[player.area].bb), &player.x, &player.y);
  regenerate_portal(player.area);
  generate_items();
 	player.type = WORLD(player.x, player.y);
  center_camera(player.x, player.y, false);
  player.energy = 1.0f;
  player.max_energy = 1.0f;
  t = time;
	setLiveVar("var", &var, 1);
}

void gamescene_render(double time){

	if (t == 0) { 
		start_game(time); 
		return;
	}

	if (camera.flying){
		if (fabs(camera.x - camera.goalx) < 0.001 && fabs(camera.y - camera.goaly) < 0.001){
			camera.x = camera.goalx;
			camera.y = camera.goaly;
			camera.flying = false;
		}
		else{
			camera.x += (camera.goalx - camera.x) * camera.speed;
			camera.y += (camera.goaly - camera.y) * camera.speed;
			//printf("cam %f %f\n", camera.x, camera.y);
		}
	}

	int alpha = 255; 
	if (poison_time > 0){
		poison_time -= 0.01;
		alpha = (int)(255 * (0.02 + (1.0 - min(1.0, poison_time) * 0.98)));
	}

	//glClear(GL_COLOR_BUFFER_BIT);
	
	// map
	for (int y = 0; y < ROWS; y++) {
		for (int x = 0; x < COLS; x++) {
			RGB col = TILE_COLORS[WORLD(x, y)];
			float sx = (x - camera.x) * TILESIZE;
			float sy = (y - camera.y) * TILESIZE;
			if (sx < -TILESIZE || sy < -TILESIZE) continue;
			if (sx >= WIDTH || sy >= HEIGHT) continue;
			drawQuad(tile_textures[WORLD(x, y)], sx, sy, TILESIZE, TILESIZE, col.r, col.g, col.b, alpha, 0.f, 0.f);
		}
	}

	//portals
	drawQuad(NULL, (portal.ax - camera.x) * TILESIZE, (portal.ay - camera.y) * TILESIZE, TILESIZE, TILESIZE, 0, 255, 0, alpha, 0.f, 0.f);
	drawQuad(NULL, (portal.bx - camera.x) * TILESIZE, (portal.by - camera.y) * TILESIZE, TILESIZE, TILESIZE, 0, 100, 0, alpha, 0.f, 0.f);

	//items
	for (int i = 0; i < NUM_ITEMS; i++) {
		if (items[i].energy <= 0) continue;
		drawQuad(NULL, (items[i].x - camera.x) * TILESIZE + 1, (items[i].y - camera.y) * TILESIZE + 1, TILESIZE - 2, TILESIZE - 2, TILE_COLORS[items[i].type].r, TILE_COLORS[items[i].type].g, TILE_COLORS[items[i].type].b, alpha, 0.f, 0.f);
	}

	//player
	drawQuad(NULL, (player.x - camera.x) * TILESIZE, (player.y - camera.y) * TILESIZE, TILESIZE, TILESIZE, player.color.r, player.color.g, player.color.b, 255, 0.f, 0.f);

	//energy bars
	for (int i = 0; i < NUM_ITEMS; i++) {
		if (items[i].energy <= 0 || items[i].type == ENERGY2) continue;
		drawQuad(NULL, (items[i].x - camera.x) * TILESIZE + 1, (items[i].y - camera.y) * TILESIZE - 4, round((TILESIZE - 2) * items[i].energy), 2, TILE_COLORS[items[i].type].r, TILE_COLORS[items[i].type].g, TILE_COLORS[items[i].type].b, alpha, 0.f, 0.f);
	}

	//player energy
	for (int i = 1; i <= (int)ceil(player.energy); ++i) {
		float energy = min( 1.0f, player.energy - (i - 1.0f) );
		drawQuad(NULL, (player.x - camera.x) * TILESIZE, (player.y - camera.y) * TILESIZE - 4 * i, round(TILESIZE * energy), 2, player.color.r, player.color.g, player.color.b, 255, 0.f, 0.f);
	}

	//minimap
	drawQuad(NULL, MINIMAP_X - 1, MINIMAP_Y - 1, COLS / MINIMAP_ZOOM + 2, ROWS / MINIMAP_ZOOM + 2, 0, 0, 0, 255, 0.f, 0.f);
	for (int y = 0; y < ROWS / MINIMAP_ZOOM; y++) {
		for (int x = 0; x < COLS / MINIMAP_ZOOM; x++) {
			RGB col = MINIMAP_COLORS[WORLD(x * MINIMAP_ZOOM , y * MINIMAP_ZOOM)];
			if (portal.ax == x * MINIMAP_ZOOM && portal.ay == y * MINIMAP_ZOOM) col = (RGB){0, 255, 0};
			else if (portal.bx == x * MINIMAP_ZOOM && portal.by == y * MINIMAP_ZOOM) col = (RGB){0, 100, 0};
			int nofog = 
				x * MINIMAP_ZOOM > camera.x && 
				x * MINIMAP_ZOOM < camera.x + COLS_IN_SCREEN && 
				y * MINIMAP_ZOOM > camera.y && 
				y * MINIMAP_ZOOM < camera.y + ROWS_IN_SCREEN ? 1 : 4;
			drawQuad(NULL, MINIMAP_X + x, MINIMAP_Y + y, 1, 1, col.r / nofog, col.g / nofog, col.b / nofog, 255, 0.f, 0.f);
		}
	}

	
	if (logo_alpha < 2.0 && logo_alpha > 0.0) logo_alpha -= 0.01;
	if (logo_alpha > 0.0){
		drawQuad(tex_logo, 12, 10, tex_logo->w, tex_logo->h, 255, 255, 255, (int)(min(1.0, logo_alpha) * 255), 0.f, 0.f);
		drawText("USE ARROW KEYS", 0, HEIGHT - 20, true, 255, 255, 255, (int)(min(1.0, logo_alpha) * 255));
	}
}
