#include <math.h>
#include "graphics.h"
#include "game_scene.h"

Texture *tex_font[26], *tex_logo;
Texture *tile_textures[3];

int main(int argc, char *argv[]){
  double time, totaltime;
  unsigned frames = 0;
  GLFWwindow* window = openWindow("WaterWorld", false, false);

/*
	HSAMPLE sample = BASS_SampleLoad(FALSE, "assets/sample.ogg", 0, 0, 3, BASS_SAMPLE_OVER_POS);

	BASS_ChannelPlay(sample, FALSE);
	
	BASS_SampleFree(sample); 
*/

  loadAtlas("assets.atlas");
  tex_logo = getTexture("logo.png");
  char letter_file[] = "_.png";
  for (char c = 'A'; c <= 'Z'; c++) {
  	letter_file[0] = c;
  	tex_font[c - 'A'] = getTexture(letter_file);
  }
	
	tile_textures[0] = getTexture("water0.png");
	tile_textures[1] = getTexture("sand0.png");
	tile_textures[2] = getTexture("grass0.png");
  
	//GLuint uniform_time = glGetUniformLocation(program, "time");

  GLuint framebuffer = createFramebuffer();

  while(!glfwWindowShouldClose(window)){
    time = glfwGetTime();
	
		updateEvents(window);	

		gamescene_processEvents(window);

		//glUniform1f(uniform_time, time);

		beginFramebuffer(framebuffer);

    gamescene_render(time);

		render();

		endFramebuffer(framebuffer);
		glfwSwapBuffers(window);
    totaltime += glfwGetTime() - time;
    frames++;
  }

 // deleteTextures();
	closeWindow();
#ifdef DEBUG
  printf("ms: %.3f | FPS: %.1f\n", totaltime / frames, (float)frames / time);
#endif
	return 0;
}
