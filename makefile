OBJ     = obj/main.o obj/graphics.o obj/hash.o obj/vendor/lodepng.o obj/game_scene.o obj/title_scene.o
HEADERS = main.c graphics.h hash.h vendor/lodepng.h game_scene.h title_scene.h vendor/bass.h
CC = gcc

DEBUG = -std=c11 -pedantic -Wall -g -D DEBUG -lm -L./ -l:libbass.so -Wl,-rpath,. 
#DEBUG = -D RELEASE -Os -finline-small-functions -lm

ifeq ($(OS),Windows_NT)
	LIBS = -lglfw3dll -lopengl32 -lglew32 -D WIN 
else 
  UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		LIBS = -lGL -lglfw -lGLEW -lm -D LINUX 
	endif
	ifeq ($(UNAME_S),Darwin)
		LIBS += -D MAC 
	endif
endif

all: waterworld

obj/vendor:
	mkdir -p obj/vendor

obj/%.o: %.c
	$(CC) -c $< -o $@ $(DEBUG)

obj/vendor/%.o: vendor/%.c 
	$(CC) -c $< -o $@ $(DEBUG)

waterworld: obj/vendor $(OBJ) $(HEADERS)
	$(CC) -o$@ $(OBJ) $(LIBS) $(DEBUG)



.PHONY: clean
clean:
	rm -f app debug
	rm -rf obj
